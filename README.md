
## About

The project is based in Laravel 8 and Innertia JS (VueJS). 


## Install 

After clone this repo run the following commands:

`composer install`

Copy and set up database values on .env file
`cp .env.example .env`

Generate key

`php artisan key:generate`

Run migrations and seeds

`php artisan migrate --seed`


Then, run serve 
`php artisan serve` and navigate :D, you can login with: 
- admin@example.com
- 12345678

Set up commands and done :D

## Commands

In order to feed the new post from source the apps include a command `php artisan blog:feedposts` that get new post and store its into database. Do not forget to add [schedule line on your sever ](https://laravel.com/docs/8.x/scheduling#running-the-scheduler) .


## Development
 
Do not forget to compile the front end (`npm install && npm run watch`) if you made any changes. 




