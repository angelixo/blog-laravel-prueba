<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\Http;
use Illuminate\Console\Command;
use App\Models\{User,Post};

class feedPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blog:feedposts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('https://sq1-api-test.herokuapp.com/posts', [
            'limit' => 5,
        ]);
        $posts = json_decode($response->body());
        foreach($posts->data as $post){
            $post = Post::create([
                'title' => $post->title, 
                'description' => $post->description,
                'publication_date' => $post->publication_date,
                'author' => '1', // 1 is ID of Admin :) 
            ]);
        }
    }

    
}
