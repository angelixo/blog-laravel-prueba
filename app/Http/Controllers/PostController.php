<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\{Post};
use Carbon\Carbon;

class PostController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
        ]);

        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'author' => auth()->id(),
            'publication_date' => Carbon::now(),
        ]);

        return Redirect::route('dashboard');
    }
}
