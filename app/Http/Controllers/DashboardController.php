<?php

namespace App\Http\Controllers;
use Inertia\Inertia;
use App\Models\Post;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $posts = Post::where('author', auth()->id())->orderBy('id', 'DESC')->get();
        return Inertia::render('Dashboard', ['posts' => $posts]);
    }
}
