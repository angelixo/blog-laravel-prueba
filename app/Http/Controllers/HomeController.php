<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Support\Facades\Route; // dev 
use Illuminate\Foundation\Application;// dev
use App\Models\{Post};


use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request){


        $posts = Post::orderBy('publication_date', $request->desc === 'asc' ? 'ASC' : 'DESC')->get();

        return Inertia::render('Welcome', [
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'posts' => $posts,
        ]);
    }
}
