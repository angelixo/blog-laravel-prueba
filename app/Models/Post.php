<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'author', 'publication_date']; // add published at 

//    protected $dates = ['deleted_at','created_at','updated_at'];

    protected $appends = ['days_ago'];

    public $timestamps = false;


    public function getDaysAgoAttribute(){
        return Carbon::parse($this->publication_date)->diffForHumans();
    }




}
