<?php

namespace Database\Seeders;
use App\Models\{User};
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Seeder;

class setUpAppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => Hash::make('12345678'),    
        ]);

        \Artisan::call('blog:feedposts');

    }
}
